import {h, render, Component } from "preact";
import bind from "bind-decorator";
import { connect } from "preact-redux";

import CurrentMedia from "./CurrentMedia";
import Playlist from "./Playlist";

class App extends Component {
	render({status, media, playlist, error}) {
		if (error) {
			return (
				<div class="blocking">
					<div class="error">{error}</div>
				</div>
			)
		}

		if (!status) {
			return (
				<div class="blocking">
					<div class="info">Establishing Connection</div>
				</div>
			)
		}

		return (
			<div class="app">
				<CurrentMedia media={this.props.media} status={this.props.status} playlist={this.props.playlist} />
				<div class="separator"></div>
				<Playlist playlist={this.props.playlist} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	if (!state) {
		return {
			status: false,
			media: false,
			playlist: false,
			error: false,
		}
	}

	return {
		status: state.status,
		media: state.media,
		playlist: state.playlist,
		error: state.error,
	}
}

export default connect(mapStateToProps)(App);