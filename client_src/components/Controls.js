import {h, render, Component } from "preact";
import bind from "bind-decorator";
import Icon from "./Icon";
import Seek from "./SeekBar";
import Volume from "./VolumeBar";
import { vlc_command, vlc_command_sequence } from "../websocket";

export default class Controls extends Component {
	render({ status, media }) {
		let state_btn;
		if (status.state === "playing") {
			state_btn = <Icon click={() => {vlc_command("pause")}}>pause</Icon>;
		} else {
			state_btn = <Icon click={() => {vlc_command("play")}}>play_arrow</Icon>;
		}

		let shuffle_btn = <Icon active={status.shuffle}
								click={() => {vlc_command("random " + ( status.shuffle ? "off" : "on"))}}
								>shuffle</Icon>;

		let repeat_btn;
		if (status.repeat) {
			repeat_btn = <Icon active
							   click={() => {vlc_command_sequence(["repeat off", "loop off"])}}
								>repeat_one</Icon>
		} else if (status.loop) {
			repeat_btn = <Icon active
							   click={() => {vlc_command_sequence(["repeat on", "loop on"])}}>repeat</Icon>
		} else {
			repeat_btn = <Icon click={() => {vlc_command_sequence(["repeat off", "loop on"])}}
								>repeat</Icon>
		}

		return (
			<div class="controls">
				<Seek status={status} />

				<div class="split-tray">
					<div>
						<Icon click={() => {vlc_command("prev")}}>skip_previous</Icon>
						{state_btn}
						<Icon click={() => {vlc_command("next")}}>skip_next</Icon>
					</div>

					<div>
						{repeat_btn}
						{shuffle_btn}
						<Volume status={status}/>
					</div>
				</div>
			</div>
		);
	}
}