import {h, render, Component } from "preact";

export default class CurrentMedia extends Component {
	constructor() {
		super();
		this.state = {
			currentId: -1,
		};
	}

	render() {
		return (
				<img src={"/art_proxy?" + (new Date()).valueOf()} />
		);
	}

	shouldComponentUpdate({playlist, status}, ) {
		let newCurrent;
		for (let item of playlist) {
			if (item.current) {
				newCurrent = item.id;
			}
		}

		if (newCurrent !== this.state.currentId) {
			this.state.currentId = newCurrent;
			return true;
		}

		/*
		When first playing a youtube video the playlist changes before information is pulled
		Thus we get art when the video is confirmed to be playing;
		 */

		if (this.props.status.time === 0 && status.time > 0) {
			return true;
		}


		return false;
	}
}