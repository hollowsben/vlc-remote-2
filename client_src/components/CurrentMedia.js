import {h, render, Component } from "preact";
import Controls from "./Controls";
import Art from "./CurrentArt";

export default class CurrentMedia extends Component {
	render({ status, media, playlist }) {
		return (
			<div class="current-media">
				<div class="pad1">
					<span class="title">{media.title}</span>
					<span class="artist">{media.artist}</span>
				</div>

				<Art status={status} playlist={playlist} />

				<Controls status={status} />
			</div>
		);
	}
}