import {h, render, Component } from "preact";

export default class Icon extends Component {
	render({ children, active, click }) {
		let classes = "icon material-icons";
		if (active) {
			classes += " active";
		}

		if (click) {
			classes += " clickable";
		}

		return (
			<i onClick={click} class={classes}>{children}</i>
		)
	}
}