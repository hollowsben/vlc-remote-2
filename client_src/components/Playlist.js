import {h, render, Component } from "preact";
import { timeString, toast } from "../util";
import Icon from "./Icon";
import { vlc_command, vlc_command_sequence } from "../websocket";
import Sortable from "sortablejs";


export default class CurrentMedia extends Component {
	constructor() {
		super();
		this.state = {
			dragged: false,
		}
	}

	renderRow(info) {
		let classes = "row";
		if (info.current) {
			classes += " current";
		}

		return (
			<div class={classes}>
				<span class="title">{info.name}</span>
				<span class="duration">{timeString(info.duration)}</span>
				<Icon click={() => {vlc_command("delete " + info.id)}}>delete</Icon>
			</div>
		)
	}

	addToPlaylist() {
		let mrl = this.state.input.value;
		vlc_command("enqueue " + mrl);
		this.state.input.value = "";
	}

	clear() {
		if (confirm("This will clear the playlist!\nProceed?")) {
			vlc_command("clear");
		}
	}

	render({ playlist }) {
		let remove;

		if (playlist.length > 0) {
			remove = <div class="playlist-clear" onClick={this.clear}>Clear Playlist</div>;
		}

		return (
			<div class="playlist">
				<div class="playlist-add">
					<span class="label">Add Media</span>
					<input type="text" placeholder="MRL" ref={(elm) => {this.state.input = elm}} />
					<Icon click={() => { this.addToPlaylist() }}>playlist_add</Icon>
				</div>

				{remove}

				<div ref={(elm) => {this.state.list = elm}}>
				{playlist.map(this.renderRow)}
				</div>
			</div>
		);
	}

	componentDidUpdate() {
		this.state.input.onkeydown = (e) => {
			if (e.keyCode === 13) {
				this.addToPlaylist();
			}
		};

		return; //ordering is broken

		let sortable = Sortable.create(this.state.list, {
			onStart: () => {
				this.state.dragged = true;
			},

			onEnd: async (e) => {
				this.state.dragged = false;

				let from = this.props.playlist[e.oldIndex].id;
				let to;

				if (e.newIndex < e.oldIndex) {
					if (e.newIndex === 0) {
						to = 2; //id 2 denotes start of playlist
					} else {
						to = this.props.playlist[e.newIndex - 1].id;
					}
					vlc_command(`move ${from} ${to}`);
				} else if (e.newIndex > e.oldIndex) {
					toast("info", "Reordering Playlist", "Stand by, this operation will take some time\nEstimated " + (e.newIndex - e.oldIndex) * 0.25 + "s");

					for (let i = e.oldIndex + 1; i <= e.newIndex; i++) {
						console.log(i);
						let from = this.props.playlist[i].id;

						let to;
						if (i === 1) {
							to = 2;
						} else {
							to = this.props.playlist[i - 2].id;
						}

						vlc_command(`move ${from} ${to}`);

						await new Promise((r) => {
							setTimeout(r, 250);
						})
					}

					toast("success", "Playlist Reordered")
				}
			}
		});
	}

	shouldComponentUpdate() {
		return !this.state.dragged;
	}
}