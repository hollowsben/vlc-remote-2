import {h, render, Component } from "preact";
import { timeString } from "../util";
import { vlc_command } from "../websocket";

export default class SeekBar extends Component {
	constructor() {
		super();
		this.state = {
			duration: -1,
			dragged: false,
		}
	}

	render({status}) {
		return (
			<div class="seek">
				<div ref={(elm) => { this.state.bar = elm }}></div>
				<div class="split-tray">
					<span ref={(elm) => { this.state.time_disp = elm }}>{timeString(status.time)}</span>
					<span>{timeString(status.length)}</span>
				</div>
			</div>
		);
	}

	componentDidUpdate() {
		let bar = this.state.bar;

		let length = this.props.status.length;
		if (length < 1) {
			length = 1;
		}

		if (!bar.noUiSlider) {
			noUiSlider.create(bar, {
				animate: true,
				start: this.props.status.time,
				range: {
					min: 0,
					max: length,
				},
				connect: [true, false],
			});

			bar.noUiSlider.on("start", () => {
				this.state.dragged = true;
			});

			bar.noUiSlider.on("end", () => {
				this.state.dragged = false;
			});

			bar.noUiSlider.on("slide", () => {
				this.state.time_disp.innerText = timeString(parseInt(bar.noUiSlider.get()));
			});

			bar.noUiSlider.on("change", () => {
				let time = parseInt(bar.noUiSlider.get());
				vlc_command("seek " + time);
			});

			this.state.dragged = false;
		} else {
			bar.noUiSlider.updateOptions({
				start: this.props.status.time,
				range: {
					min: 0,
					max: length,
				}
			});
		}
	}

	shouldComponentUpdate({status}) {
		if (status.length !== this.state.duration) {
			this.state.duration = status.length;
			return true;
		}

		if (!this.state.dragged) {
			if (this.state.bar.noUiSlider.get() !== status.time) {
				this.state.bar.noUiSlider.set(status.time);
				this.state.time_disp.innerText = timeString(status.time)
			}
		}

		return false;
	}
}