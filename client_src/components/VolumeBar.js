import {h, render, Component } from "preact";
import bind from "bind-decorator";
import { timeString } from "../util";
import Icon from "./Icon";
import { vlc_command } from "../websocket";

export default class VolumeBar extends Component {
	constructor() {
		super();
		this.state = {
			init: false,
			dragged: false,
		}
	}

	render({status}) {
		return (
			<div class="volume">
				<Icon>volume_mute</Icon>
				<div class="bar">
					<div ref={(elm) => { this.state.bar = elm }}></div>
				</div>
			</div>
		);
	}

	componentDidUpdate({status}) {
		let bar = this.state.bar;
		if (!bar.noUiSlider) {
			noUiSlider.create(bar, {
				animate: true,
				start: status.volume,
				range: {
					min: 0,
					max: 200,
				},
				connect: [true, false],
			});

			bar.noUiSlider.on("start", () => {
				this.state.dragged = true;
			});

			bar.noUiSlider.on("end", () => {
				this.state.dragged = false;
			});

			bar.noUiSlider.on("change", () => {
				let time = parseInt(bar.noUiSlider.get());
				vlc_command("volume " + time);
			});

			this.state.dragged = false;
		} else {
			bar.noUiSlider.updateOptions({
				start: this.props.status.time,
				range: {
					min: 0,
					max: this.props.status.length,
				}
			});
		}
	}

	shouldComponentUpdate({status}) {
		if (!this.state.init) {
			this.state.init = true;
			return true;
		}

		if (!this.state.dragged) {
			if (this.state.bar.noUiSlider.get() !== status.volume) {
				this.state.bar.noUiSlider.set(status.volume);
			}
		}

		return false;
	}
}