import {h, render, Component } from "preact";
import App from "./components/App";
import { Provider } from "preact-redux";
import store from "./store";
import { initWs, bindWs } from "./websocket";

require("preact/devtools");


initWs();

bindWs("status", (response) => {
	store.dispatch({
		type: "UPDATE_STATUS",
		status: response.status
	});
});

const provider = (
	<Provider store={store}>
		<App />
	</Provider>
);

render(provider, document.getElementById("root"));