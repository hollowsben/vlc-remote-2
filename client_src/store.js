import { createStore } from "redux";

const reducer = (state = [], action) => {
	if (action.type === "UPDATE_STATUS") {
		return action.status;
	}
};

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

document.redux_dispatch = store.dispatch;

export default store