export function timeString(sec_num) {
	if (sec_num < 0) {
		return "??:??";
	}

	let hours   = Math.floor(sec_num / 3600);
	let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
	let seconds = sec_num - (hours * 3600) - (minutes * 60);

	if (hours !== 0 && minutes < 10) {
		minutes = '0' + minutes;
	}

	if (seconds < 10) {seconds = "0"+seconds;}
	//return hours+':'+minutes+':'+seconds;
	return (hours || '') + (hours ? ':' : '') + minutes + ':' + seconds;
}

export function toast(type, var1, var2) {
	let title, body;

	let options = {
		progressBar: true,
	};

	if (var2) {
		title = var1;
		body = var2;
		toastr[type](body, title, options);
	} else {
		body = var1;
		toastr[type]("", body, options);
	}
}