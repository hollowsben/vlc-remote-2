import { toast } from "./util.js";

export function initWs() {
	const addr = `ws://${window.location.host}`;
	let ws = new WebSocket(addr);

	ws.onmessage = (messageData) => {
		let message = {
			detail: JSON.parse(messageData.data)
		};

		let messageEvent = new CustomEvent("ws_message", message);
		document.dispatchEvent(messageEvent);
	};

	ws.onerror = (e) => {
		toast("error", "WebSocket Error", e);
	};

	ws.onclose = () => {
		toast("error", "WebSocket closed!", "Attempting to reconnect");
		reconnect(5);
	};

	document.ws = ws;
	return ws;
}

function reconnect(t) {
	t = t - 1;

	if (!t) {
		document.redux_dispatch({
			type: "UPDATE_STATUS",
			status: {
				error: "Connection to server lost"
			}
		});
		return;
	}

	let ws = initWs();
	let nerror = ws.onerror;
	let nclose = ws.onclose;

	ws.onerror = () => {
		toast("error", "Recconnection Failed!", `Could not connect to websocket\n${t} ${(t === 1 ? "try" : "tries")} remaining`);
		setTimeout(() => { reconnect(t) }, 5000);
	};

	ws.onclose = undefined;

	ws.onopen = () => {
		toast("success", "Connection Established", "The connection to the websocket was reestablished");
		ws.onerror = nerror;
		ws.onclose = nclose;
	};
}

export function bindWs(type, callback) {
	document.addEventListener("ws_message", (message) => {
		if (message.detail.type === type) {
			callback(message.detail);
		}
	});
}

export function vlc_command(command) {
	document.ws.send(JSON.stringify({
		type: "command",
		command
	}))
}

export function vlc_command_sequence(commands) {
	document.ws.send(JSON.stringify({
		type: "command_sequence",
		commands
	}))
}