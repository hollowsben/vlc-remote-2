const fetch = require("node-fetch");
const base64 = require("base-64");

module.exports = class HttpInterface {
	constructor(host, config) {
		this.config = config;
		this.config.host = host;

		this.url = "http://" + this.config.host + ':' + this.config.port + "/requests/";

		if (this.config.password !== "") {
			this.headers = { "Authorization" : `Basic ${base64.encode(':' + this.config.password)}`};
		} else {
			this.headers = {};
		}
	}

	async getStatus() {
		let status_r = await fetch(this.url + "status.json", { headers: this.headers });
		let s = await status_r.json();

		let playlist_r = await fetch(this.url + "playlist.json", { headers: this.headers });
		let p = await playlist_r.json();

		// Clean up before sending
		let status = {};
		status.length = s.length;
		status.loop = s.loop;
		status.shuffle = s.random;
		status.repeat = s.repeat;
		status.state = s.state;
		status.time = s.time;
		status.volume = s.volume;

		let media = {};
		if (!s.information) {
			// No media is playing
			media.artist = "";
			media.description = "";
			media.title = "[No Media Playing]";
		} else {
			let m = s.information.category.meta;
			media.artist = m.artist;
			media.description = m.description;
			media.title = m.title;
		}
		let playlist = [];
		let pl = p.children[0].children;

		for (let i of pl) {
			let item = {};
			item.duration = i.duration;
			item.id = i.id;
			item.name = i.name;
			item.current = i.current;
			playlist.push(item);
		}

		return {status, media, playlist}
	}
};