const http = require("http");
const express = require("express");
const fs = require("fs");
const WS = require("ws");
const Interface = require("./interface");
const fetch = require("node-fetch");
const base64 = require("base-64");

/* Promise Stacktraces */
process.on('unhandledRejection', (reason, p) => {
	console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
	console.log(reason.stack);
	// application specific logging, throwing an error, or other logic here
});

/* Load Config */
if (!fs.existsSync("config.json")) {
	let exampleConfig = fs.readFileSync("config.example.json");
	fs.writeFileSync("config.json", exampleConfig);
}

const config = JSON.parse(
	fs.readFileSync("config.json", { encoding: "utf-8" })
);

/* Create HTTP Server */
const app = express();
app.use("/", express.static("client"));
app.get("/art_proxy", (req, res) => {
	let headers;
	if (config.vlc.http.password !== "") {
		headers = { "Authorization" : `Basic ${base64.encode(':' + config.vlc.http.password)}`};
	} else {
		headers = {};
	}
	fetch(`http://${config.vlc.host}:${config.vlc.http.port}/art`, {headers})
		.then((fres => {
			fres.body.pipe(res);
		}));
});

const server = http.createServer(app);

/* Create WebSocket Server */
const wss = new WS.Server({ server });

/* Create VLC interface */
const interface = new Interface(config.vlc, wss);

/* Start Server */
server.listen(config.port);
console.log(`Listening on port ${config.port}`);


