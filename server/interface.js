const uuid = require("uuid/v4");
const HttpInterface = require("./http-interface");
const TelnetInterface = require("./telnet-interface");

module.exports = class Interface {
	constructor(config, wss) {
		this.updateStatus = this.updateStatus.bind(this);

		this.config = config;
		this.http = new HttpInterface(config.host, config.http);
		this.telnet = new TelnetInterface(config.host, config.telnet);

		this.websockets = {};

		wss.on("connection", (ws) => {
			ws.id = uuid();
			this.websockets[ws.id] = ws;

			ws.on("close",  () => {
				delete this.websockets[ws.id];
			});

			ws.on("message", async (message) => {
				message = JSON.parse(message);
				if (message.type === "command") {
					this.telnet.send(message.command);
				} else if (message.type === "command_sequence") {
					for (let command of message.commands) {
						await this.telnet.send(command);
					}
				}
			});
		});

		setInterval(this.updateStatus, this.config.update_interval_ms);
	}

	async updateStatus() {
		let status = await this.http.getStatus();

		let message = JSON.stringify({
			type: "status",
			status
		});

		for (let ws of Object.values(this.websockets)) {
			this.sendWs(ws, message);
		}
	}

	async sendWs(ws, message) {
		try {
			await ws.send(message)
		} catch (e) {
			console.error("Failed to send message to websocket");
			console.error(e);
		}
	}
};