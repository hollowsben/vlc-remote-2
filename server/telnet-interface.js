const TelnetClient = require("telnet-client");

module.exports = class TelnetInterface {
	constructor(host, config) {
		let params = {
			host,
			port: config.port,
			password: config.password,
			shellPrompt: '> ',
			echoLines: 0,
			reconnectInterval: 5000,
			timeout: 2000,
		};

		this.connection = new TelnetClient();

		this.connection.connect(params)
			.then(() => {console.log("Telnet connection established")});

		this.connection.on("timeout", () => {
			console.error("Telnet connection timed out");
		});

		this.connection.on("close", () => {
			console.log("Telnet connection closed");
		});
	}

	async send(command) {
		try {
			await this.connection.exec(command);
		} catch (e) {
			console.error("Failed to send command:\n" + e);
		}
	}
};