module.exports = {
	entry: ["babel-polyfill", __dirname + "/client_src/index.js"],
	output: {
		path: __dirname + "/client",
		filename: "bundle.js",
	},
	devtool: 'source-map',
	module: {
		loaders: [
			{test: /\.jsx?$/, loader: "babel-loader"},
		]
	},
};